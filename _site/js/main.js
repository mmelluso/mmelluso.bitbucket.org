

var svgMorpheus;

window.onload = function () {

  svgMorpheus = new SVGMorpheus('#logos', {duration: 450, easing: 'circ-in-out', rotation: 'none'});

  if( $('.projects--nav a.active').length > 0 ){
    var id = $('.projects--nav a.active').data('svg');
    svgMorpheus.to(id);
    if($('.projects--nav a.active').parent().is(':first-child')){
      $('.fold--previous').addClass('inactive');
    }
  }

}

function toggleContent(actual, previous, next) {
  /* load and show new content */
  $('.fold--content').load(actual+' .fold--content > *', function(event){
    //console.log(url);
    //$('body').addClass('overflow-hidden');
    $('body').addClass('fold__open');
    $(this).removeClass('hide');

    if(previous){
      $('.fold--previous').attr('href', previous);
      $('.fold--previous').removeClass('inactive');
    } else {
      $('.fold--previous').addClass('inactive');
    }

    if(next){
      $('.fold--next').attr('href', next);
      $('.fold--next').removeClass('inactive');
    } else {
      $('.fold--next').addClass('inactive');
    }
  });
}

function viewportSize() {
  /* retrieve the content value of .cd-main::before to check the actua mq */
  //return window.getComputedStyle(document.querySelector('.cd-main'), '::before').getPropertyValue('content').replace(/"/g, "").replace(/'/g, "");
  return 'desktop';
}
//(function($) {

  $(document).ready(function(){
      $('.projects--nav').hover(function(){}, function(){
        if(!$('body').hasClass('fold__open')){
          svgMorpheus.to('triangle')
        }
      })

      $('.projects--nav a').hover(function(){
        if(!$('body').hasClass('fold__open')){
          var id = $(this).data('svg');
          svgMorpheus.to(id);
        }
      })

      /* open folding content */
      $('.projects--nav a').on('click', function(event){
        event.preventDefault();
        var actual = $(this).attr('href');
        var actualID = actual.split('.html')[0];
        var previous = $(this).parent().prev().find('a').attr('href');
        var next = $(this).parent().next().find('a').attr('href');
        if(!$('body').hasClass('fold__open')){
          //console.log(actual + previous + next);
          toggleContent(actual, previous, next);
          $(this).addClass('active');
        } else {
          $('.fold--content').addClass('hide');
          setTimeout(function () {
              toggleContent(actual, previous, next);
          }, 300);
          setTimeout(function () {
              svgMorpheus.to(actualID);
          }, 750);
          $('.projects--nav a.active').removeClass('active');
          $(this).addClass('active');
        }
      });

      /* change folding content */
      $('.fold--previous, .fold--next').on('click', function(event){
        event.preventDefault();
        if(!$(this).hasClass('inactive')){
          $('.fold--content').addClass('hide');
          var actual = $(this).attr('href');
          var actualID = actual.split('.html')[0];
          var previous = $('.projects--nav a[data-svg="'+actualID+'"]').parent().prev().find('a').attr('href');
          var next = $('.projects--nav a[data-svg="'+actualID+'"]').parent().next().find('a').attr('href');
          //console.log(actual + previous + next);
          setTimeout(function () {
              toggleContent(actual, previous, next);
          }, 300);
            setTimeout(function () {
                svgMorpheus.to(actualID);
            }, 750);
          $('.projects--nav a.active').removeClass('active');
          $('.projects--nav a[data-svg="'+actualID+'"]').addClass('active');
        }
      });

      /* close folding content */
      $('.fold--close').on('click', function(event){
        event.preventDefault();
        $('.projects--nav a.active').removeClass('active');
        //$('body').removeClass('overflow-hidden');
        $('body').removeClass('fold__open');
        svgMorpheus.to('triangle');
      });

      /* about section */
      var poly_stagger  = 0.00475,
          poly_duration = 1,
          facePaths,
          poly_staggerFrom = {
            scale: 0,
            opacity: 0,
            transformOrigin: 'center center',
          },
          poly_staggerTo = {
            opacity: 1,
            scale: 1,
            ease: Elastic.easeInOut
          };

      $('.about--link').on('click', function(event){
        event.preventDefault();
        if(!$('body').hasClass('transition') && !$('body').hasClass('fold__open')){
          $('body').addClass('transition');
          if(!$('body').hasClass('about-open')){
            var tmax_tl       = new TimelineMax(),
                poly_shapes   = $('#face > path');
            facePaths = $('#face').html();

            $('body').addClass('about-open');
            setTimeout(function () {
              $('.about--logo svg').addClass('visible');
              tmax_tl.staggerFromTo(poly_shapes, poly_duration, poly_staggerFrom, poly_staggerTo, poly_stagger, 0);
              $('body').removeClass('transition');
            }, 1201);
          } else {
            $('body').removeClass('about-open');
            $('#face').html(facePaths);
            setTimeout(function () {
                $('.about--logo svg').removeClass('visible');
                $('body').removeClass('transition');
            }, 1201);
          }
        } else if($('body').hasClass('fold__open')){
          $('.fold--close').click();
          setTimeout(function () {
            $('.about--link').click();
          }, 400);
        }
      })

  })

//})(jQuery);
